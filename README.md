# Techlit recovery custom iso bootstrapper

Collection of scripts and configuration files to build a custom iso for Techlit
using the Artix linux's [buildiso](https://wiki.artixlinux.org/Main/InstallationUsingBuildiso)

## License This project is licensed under the GPL-2.0-or-later License

See the [LICENSE](./LICENSE) file for more details.

## Requirements

### Install dependencies

> artools, iso-profiles, sbsigntools, [yay, grub-silent(2-06-6), bdf-unifont](aur)

Currently to build you'll require either Artix or Arch Linux installed with
[admin](https://gitlab/techlit-africa/admin) installed at `/opt/admin`.

If you're on Artix then you simply need to install artools and iso-profiles

`yay -S artools-{base,iso,pkg} iso-profiles sbsigntools bdf-unifont`
To launch the iso you'll need qemu run: `sudo pacman -S qemu-base`

## Setup

1. Clone this repo
2. Symlink/copy this directory to `$HOME/recovery` or change `WORKSPACE_DIR` in config
3. Install dependencies

  ```bash
  yay -S artools-{base,iso,pkg} iso-profiles sbsigntools bdf-unifont
  ```

4. Create the build directory or specify a different build path in the script

  ```bash
  sudo mkdir -p /var/lib/artools/buildiso/artix/base
  ```

5. Run `tl-bootstrap-recovery` to build the iso.

## Directory structure

* bin
  * `tl-bootstrap-recovery` - Sets up a chroot env and runs all the scripts below
  * `pre-bake` - makes changes to boot before baking the iso
  * `launch-qemu` - start the built iso using qemu emulator
* artools
  * Artools config files can be found here config directory can be copied or symlinked
  either the user's home directory or the system-wide configuration directory.
  * $HOME/.config/artools [ user ] or  /etc/artools [ system ]
  * pacman config shoud go to artools/pacman.conf.d/iso-x86-64.conf

* iso-profiles
  * Copy/symlink this directory to $HOME/artools-workspace
  * Base profile config files used by artools, changes made here end up in the rootfs
* configs
  * config files copied to live system

## Available feature flags

1. `BASE`: Path to build, defaults to `/var/lib/artools/buildiso/base`
(Also check `CHROOTS_DIR` default [/var/lib/artools] in artools config)
2. `KERNEL`: Can be `linux`(default) or `linux-t2`
3. `KEYS`: Signing keys directory, defaults to `./keys`
4. `SHIM_PATH`: The path to the shim (shim-signed) binaries default
`/usr/share/shim`
5. `SECURE` or `SECURE_ISO`: Enable Secure Boot
6. `RAM` or `COPYTORAM`: Enable `copytoram` boot param
