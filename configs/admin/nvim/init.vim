set nocompatible
set t_Co=256
" No gui!
" set termguicolors

set ttyfast
set lazyredraw

set encoding=utf8
set ffs=unix

set nobackup
set nowritebackup
set noswapfile

filetype on
syntax on

set re=0

set cursorline
set cursorcolumn
hi clear CursorLine
hi clear CursorColumn
hi CursorLine term=reverse ctermbg=234
hi CursorColumn term=reverse ctermbg=234

set showmode
set laststatus=1
set showcmd

filetype plugin on
filetype indent on

set autoindent
set smartindent

set smarttab
set expandtab

set listchars=tab:░▒,trail:·,eol:‾,nbsp:·
set list

set backspace=2

" How many spaces to use for >> commands
set shiftwidth=2
" How many spaces are in a Tab
set tabstop=2
" Only affects editing ?
set softtabstop=2

" Don't ever wrap lines
set nowrap

set mouse=a
" " a mousy hack
" if has("mouse_sgr")
"   set ttymouse=sgr
" else
"   set ttymouse=xterm2
" end

set ruler
set cmdheight=1

"set nonumber
set number
set relativenumber

set wildmenu

set ignorecase
set smartcase
set hlsearch
set incsearch
set magic

set statusline=\ %F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l\ \ Column:\ %c

" delete trailing whitespace
func! DeleteTrailingWhitespace()
  exe "normal mz"
  %s/\s\+$//ge
  exe "normal `z"
endfunc
autocmd BufWrite * :call DeleteTrailingWhitespace()

let mapleader=","

" redo
nnoremap <leader>u :redo

" write buffer
nnoremap <leader><leader> :w<Return>
nnoremap <leader>w :w<Return>
nnoremap <leader>W :w!<Return>

" kill buffer
nnoremap <leader>q :q<Return>
" super kill buffer
nnoremap <leader>Q :q!<Return>

" edit vimrc
nnoremap <leader>v :vsplit ~/.vimrc<Return>
" reload vimrc
nnoremap <leader>V :so ~/.vimrc<Return>

""" Panes
" focus
nnoremap <leader>a <C-W>h
nnoremap <leader>s <C-W>j
nnoremap <leader>d <C-W>k
nnoremap <leader>f <C-W>l

" move
nnoremap <leader>A <C-W>H
nnoremap <leader>S <C-W>J
nnoremap <leader>D <C-W>K
nnoremap <leader>F <C-W>L

" new above (blank, via edit, ranger, ctrlp)
nnoremap <leader>G :split<Return><C-W>j
nnoremap <leader>ge :split<Return><C-W>j:e

" new right (blank, via edit, ranger, ctrlp)
nnoremap <leader>H :vsplit<Return><C-W>l
nnoremap <leader>he :vsplit<Return><C-W>l:e

""" Tabs
" focus
nnoremap <leader>. :tabnext<Return>
nnoremap <leader>m :tabprev<Return>

" new (blank, via edit, ranger, ctrlp)
nnoremap <leader>N :tabnew<Return>
nnoremap <leader>ne :tabnew<Return>:e

" create, open, close fold
vnoremap <leader>z :fold<Return>
vnoremap <leader>x :foldopen<Return>
vnoremap <leader>c :foldclose<Return>

" turn highlighting on and off
nnoremap <leader>3 :set hlsearch!<Return>
" turn list mode on and off
nnoremap <leader>4 :set list!<Return>
" turn number mode on and off
nnoremap <leader>1 :set number!<Return>
" turn relativenumber mode on and off
nnoremap <leader>2 :set relativenumber!<Return>
" turn expandtab on and off
nnoremap <leader><Tab> :set expandtab!<Return>
" show digraphs
nnoremap <leader>` :digraphs<Return>

" surround things
nnoremap <leader>"  viw<Esc>a"<Esc>bi"<Esc>lel
nnoremap <leader>'  viw<Esc>a'<Esc>bi'<Esc>lel
nnoremap <leader>;  viw<Esc>a)<Esc>bi(<Esc>lel
nnoremap <leader>:  viw<Esc>a]<Esc>bi[<Esc>lel
nnoremap <leader>\  viw<Esc>a><Esc>bi<<Esc>lel
nnoremap <leader>\| viw<Esc>a}<Esc>bi{<Esc>lel
nnoremap <leader>#  viw<Esc>a}<Esc>bi#{<Esc>lel
nnoremap <leader><  viw<Esc>a`<Esc>bi`<Esc>lel

" move lines
nnoremap <C-j> <Esc>:m .+1<Return>==
nnoremap <C-k> <Esc>:m .-2<Return>==
vnoremap <C-j> <Esc>:m '>+1<Return>gv=gv
vnoremap <C-k> <Esc>:m '<-2<Return>gv=gv

" escape quickly
imap jj <Esc>

" ColorScheme
" colorscheme codedark
