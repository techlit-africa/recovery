#!/hint/bash
# shellcheck disable=SC2034,1093
#
if [[ $- =~ i ]]; then
  echo "${BASH_SOURCE[0]} is not meant for interactive use" >&2
  echo "Refusing to continue" >&2
  return 1
fi

# use admin lib
ADMIN_LIB="${ADMIN_LIB:-/opt/admin/lib/lib.bash}"
# shellcheck source=/opt/admin/lib/lib.bash
[ -f "$ADMIN_LIB" ] && source "$ADMIN_LIB" || echo "$ADMIN_LIB" not found please install admin

# use makepkg's message library
MAKEPKG_LIBRARY=${MAKEPKG_LIBRARY:-/usr/share/makepkg}
# shellcheck source=/usr/share/makepkg/util/message.sh
source "${MAKEPKG_LIBRARY}"/util/message.sh
colorize

TERM=xterm-256color

ROOT="$(realpath "$(dirname "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")")"
source "$(realpath "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")")/conf"

set -eETo pipefail

# When using getopts, print errors
OPTERR=1

#
## Pretty printers
#
err() {
  local mesg=$1
  shift
  echo -e "${RED}${BOLD}❌❌❌ ${mesg}${ALL_OFF}\n" "$@" >&2
}

run() {
  ((QUIET)) && return
  echo -e "${MAGENTA}${BOLD}:: ==> ${ALL_OFF} ${GREEN}$*${ALL_OFF}"
  "$@"
  local status=$?
  if [ $status -ne 0 ]; then
    err "$* Failed with error code: $status"
  fi
  return $status
}

msg2() {
  ((QUIET)) && return
  local mesg=$1
  shift
  printf "${YELLOW}  ->${ALL_OFF}${BOLD} ${mesg}${ALL_OFF}\n" "$@"
}

#
## Filesystem helpers
#
tl-root-install-dir() {
  [ -d "$DEST/$5" ] && sudo rm -rf "${DEST:?}/$5"
  sudo mkdir -p "$DEST/$5"

  (
    cd "$SRC/$1" || false
    sudo find . -type f -exec sudo install -o"$2" -g"$3" -m"$4" -vD "{}" "$DEST/$5/{}" \;
  )
  run sudo chown -R "$2:$3" "$DEST/$5"
}

# Initialize pacman with arch repos
# USAGE: init-pacman
init-pacman() {
  PREFIX="$ROOTFS"
  root-chroot "sed -i 's/^CheckSpace/# CheckSpace/' /etc/pacman.conf"

  # Signature from "Artix Buildbot" is invalid
  run root-chroot "pacman-key --init"
  run root-chroot "pacman-key --populate artix"

  # Arch extras
  if [ ! -f "$ROOTFS"/var/lib/pacman/sync/extra.db ]; then
    run root-chroot "pacman -Sy --noconfirm --needed \
      artix-archlinux-support archlinux-mirrorlist gpgme \
      && pacman-key --populate archlinux"

    # Uncomment selected mirrorlist
    COUNTRIES=("Kenya" "Netherlands" "Germany")
    for COUNTRY in "${COUNTRIES[@]}"; do
      run sudo sed -i "/## $COUNTRY/,/^$/s/^#//" \
        "$ROOTFS/etc/pacman.d/mirrorlist-arch"
    done

    SRC="$ROOT"/configs/system/ DEST="$ROOTFS"/etc/
    run tl-root-install pacman.conf 0 0 755 pacman.conf

    #NOTE: root-chroot not working here why?
    run sudo artix-chroot "$ROOTFS" pacman -Sy --noconfirm --disable-download-timeout
  fi
}

tl-aur-install() {
  run yay -S "$1" --noconfirm --builddir "$ROOT/builds" \
    --cleanafter --asexplicit --removemake --config \
    "$ROOT"/configs/system/pacman.conf \
    --norebuild --root "$ROOTFS"
}

tl-pacman-install() {
  run root-chroot "pacman -S --noconfirm --needed $@"
}
